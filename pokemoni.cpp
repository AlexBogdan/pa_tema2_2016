// Copyright 2017 Andrei Bogdan Alexandru

#include <iostream>
#include <fstream>
#include <cmath>
#include <cstring>
#include <list>
#include <utility>
#include <stack>
#include <queue>
#include <vector>
#include <climits>
#include <algorithm>
#include <cctype>

#define MAX_NODES 100
#define MAX_POKEMONS 20

using namespace std;

int best = 1;

// Node = Oras
class Node {
 public:
	int value;  // Numarul orasului
	list<Node*> parents;  // Orasele din care am venit
	list<Node*> children;  // Orasele inspre care mergem
	bool discovered = false;  // Marcheaza daca am trecut prin orasul curent
	bool marked = false;  // Marcheaza daca orasul curent este in drumul minim
	// Cate orase am parcurs pana in orasul curent
	unsigned long long shortestPath = ULLONG_MAX;
	long numberOfPaths = 0;  // Pe cate drumuri se putea ajunge in orasul curent

	Node(): value(0) {}

	void setValue(int value) {
		this->value = value;
	}
	
	void print() {
		cout << value << " : Lungime drum min = " << shortestPath
			 << " | Numar drumuri = " << numberOfPaths << " | Parinti = [";
		for (Node* n : parents) {
			cout << n->value << " ";
		}
		cout << "]";
		cout << " Copii = [";
		for (Node* n : children) {
			cout << n->value << " , ";
		}
		cout << "]" << " |  Marked = " << marked << endl;
	}
};

class Compare {
 public:
    bool operator() (Node* node1, Node* node2) {
        return node1->shortestPath >= node2->shortestPath;
    }
};

//   Tine minte drumurile pentru un anumit pokemon, vom avea in main un vector
// de Graph pentru a separa informatiile fiecarui pokemon
class Graph {
 public:
 	int N;
	Node *nodes;
	int **edges;

	Graph(int N) {
		this->N = N;
		nodes = new Node[N+1];
		edges = new int*[N+1];
		for (int i = 0; i <= N; ++i) {
			nodes[i].setValue(i);
			edges[i] = new int[N+1];
			for (int j = 0; j <= N; ++j) {
				edges[i][j] = 0;
			}
		}  // Initializam cele N orase cu indicii corespunzatori
	}

	Node getNodeByIndex(int i) {
		return nodes[i];
	}

	//   Salvam intotdeauna muchia minima primita (in cazul in care exista mai
	// multe drumuri posibile intre 2 orase)
	void addEdge(int cost, int node1, int node2) {
		if (edges[node1][node2] == 0 || edges[node1][node2] > cost) {
			edges[node1][node2] = edges[node2][node1] = cost;
		}
	}
	
	/* 
		Cautam drumurile minime folosind Dijkstra
	*/
	void findMinimumPaths(int start) {
		Node *current = &nodes[start];
		current->shortestPath = 0;
		current->discovered = true;

		vector<bool> finished (N+1, false);
		priority_queue<Node*, vector<Node*>, Compare> q;
		q.push(current);
		finished[start] = true;

		while(! q.empty()) {
			current = q.top();
			for (int i = 1; i <= N; ++i) {
				if (edges[current->value][i] != 0 && finished[i] == false) {
					if (nodes[i].discovered == false) {
						nodes[i].discovered = true;
						q.push(&nodes[i]);
					}
					if (nodes[i].shortestPath >
							current->shortestPath + edges[current->value][i]) {
						nodes[i].shortestPath = 
							current->shortestPath + edges[current->value][i];
						nodes[i].parents.clear();
						current->children.push_back(&nodes[i]);
					}
					if (nodes[i].shortestPath ==
							current->shortestPath + edges[current->value][i]) {
						// Am gasit un drum nou de acelasi cost
						nodes[i].parents.push_back(current);
						current->children.push_back(&nodes[i]);
					}
				}
			}
			finished[current->value] = true;
			q.pop();
		}
	}

	void printPath(int start) {
		nodes[start].print();
		if (! nodes[start].parents.empty()) {
			printPath(nodes[start].parents.front()->value);
		}
	}

	// //  Marcheaza toate drumurile pe care putem merge din orasul N in orasul 1
	// void markPaths(int index) {
	// 	nodes[index].marked = true;
	// 	for (Node* n : nodes[index].parents) {
	// 		markPaths(n->value);
	// 	}
	// }

	// //   Vom parcurge drumurile minime obtinute si vom marca orasele prin care
	// // am trecut, calculand si numarul de drumuri posibile pe acest drum
	// void generatePaths(int current) {
	// 	for (Node* n : nodes[current].children) {
	// 		if (n->marked == true) {
	// 			nodes[n->value].numberOfPaths += 
	// 				nodes[current].numberOfPaths * edges[n->value][current];
	// 		}
	// 		//cout << "Nodul " << current << " : " << n->numberOfPaths << " " << edges[n->value][current] << endl;
	// 	}
	// 	for (Node* n : nodes[current].children) {
	// 		generatePaths(n->value);
	// 	}
	// }
	
	// // Cauta cel mai bun oras
	// long long getNumberOfRoads(int current) {
	// 	long long inEdges = 0, outEdges = 0, alteleIn = 0, alteleOut = 0;
	// 	for (Node* n : nodes[current].children) {
	// 		if (n->marked == true) {
	// 			outEdges += edges[current][n->value];
	// 		}
	// 		else {
	// 			alteleOut += edges[current][n->value];
	// 		}
	// 	}

	// 	for (Node* n : nodes[current].parents) {
	// 		if (n->marked == true) {
	// 			inEdges += edges[current][n->value] * n->numberOfPaths;
	// 		}
	// 		else {
	// 			alteleIn += edges[current][n->value] * n->numberOfPaths;
	// 		}
	// 	}
		
	// 	if (current == N || current == 1) {
	// 		return inEdges + outEdges;
	// 	}
	// 	else {
	// 		return outEdges * nodes[current].numberOfPaths * 2 + alteleIn * outEdges + alteleOut * nodes[current].numberOfPaths;
	// 	}
	// }

	void print() {
		for (int i = 1; i <= N; ++i) {
			nodes[i].print();
		}
	}
};

int main() {
/* 
	-=-=-=-=-=-=-=-=-= Declarari + citiri -=-=-=-=-=-=-=-=-=-=-=
		Vom salva P grafuri, cate unul pentru fiecare pokemon, in care vom salva
	costurile muchiilor.
*/
	ifstream in("pokemoni.in");
	ofstream out("pokemoni.out");
	int N, M, P, K, x, y, cost;
	
	in >> N >> M >> P >> K;
	vector<Graph> graphs;
	for (int p = 0; p <= P; ++p) {
		Graph graph(N);
		graphs.push_back(graph);
	}  // Ne declaram cate un graf pentru fiecare pokemon

	for (int i = 0; i < M ; ++i) {
		in >> x >> y;
		for (int p = 1; p <= P; ++p) {
			in >> cost;
			graphs[p].addEdge(cost, x, y);
		}
	}
	in.close();
	
	unsigned long long aux, result = ULLONG_MAX;

	// Aplicam Dijkstra pe fiecare graf si alegem cel mai bun raspuns
	if (K == 0) { 
		for (int p = 1 ; p < P ; ++p) {
			graphs[p].findMinimumPaths(1);
			aux = graphs[p].nodes[N].shortestPath;
			if (aux < result) {
				result = aux;
			}
			cout << "Avem in graful " << p << " -- " << graphs[p].edges[1][6] << endl; 
			//cout << "Pentru pokemonul " << p << " avem : " << aux << endl;
		}
	}
	graphs[1].printPath(N);

	out << result;
	out.close();

	// graphs[1].print();
	// cout << endl << endl << endl;
	// graphs[2].print();
	// cout << endl;
	

	// graph.markPaths(1);
	// graph.generatePaths(N);

	// //graph.print();
	// //printf ("\n%ld\n%.3f\n", graph.getNodeByIndex(1).numberOfPaths,  graph.getBestScore());
	// fprintf (out, "%ld\n%.3f", graph.getNodeByIndex(1).numberOfPaths,  graph.getBestScore());

	// graph.getNodeByIndex(best).print();
	// printf("\n\nNodul %d : %.3f\n", 2 , (float) graph.getNumberOfRoads(best) / graph.getNodeByIndex(1).numberOfPaths);

	return 0;
}
