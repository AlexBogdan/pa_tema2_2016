// Copyright 2017 Andrei Bogdan Alexandru

#include <iostream>
#include <fstream>
#include <cmath>
#include <cstring>
#include <list>
#include <utility>
#include <stack>
#include <vector>
#include <climits>
#include <algorithm>
#include <cctype>

#define MAX_NODES 26  // Nu putem avea mai mult de 26 de litere

using namespace std;

// Node va reprezenta o litera din alfabetul standard
class Node {
 public:
	char value;
	list<Node*> neighbours;
	int discoverTime = 0;
	bool discovered = false;

	Node(): value(' ') {}

	void setValue(char value) {
		this->value = value;
	}

	//	 Verifica daca nodul primit ca parametru se afla in lista de vecini ai
	// nodului curent
	bool isNeighbour(Node *node) {
		for (auto n : neighbours) {
			if (n->value  == node->value) {
				return true;
			}
		}
		return false;
	}

	// Adauga un vecin nou doar daca acesta nu este deja in lista
	void addNeighbour(Node *node) {
		if (! isNeighbour(node)) {
			neighbours.push_back(node);
		}
	}
};

// Comparator pentru sortarea literelor conform noului alfabet
bool discoverTimeComp(Node &a, Node &b) {
	return a.discoverTime >= b.discoverTime;
}

//   Tinem minte constrangerile pe care le obtinem din compararea cuvintelor
// primite
class Graph {
 public:
	Node nodes[MAX_NODES];

	Graph() {
		for (int i = 0; i < MAX_NODES; ++i) {
			nodes[i].setValue('a' + i);
		}  // Initializam cele N noduri ca fiind literele din alfabet
	}

	Node getNodeByIndex(int i) {
		return nodes[i];
	}

	Node getNodeByValue(char value) {
		return nodes[value - 'a'];
	}

	void addEdge(char value1, char value2) {
		nodes[value1 - 'a'].addNeighbour(&(nodes[value2 -'a']));
	}

	//   Vom aplica DFS din nodul primit si vom atribui nodurilor pe care le vom
	// gasi prin parcurgere cate un discoverTime corespunzatori
	int dfs(Node *start, int lastDiscoverTime) {
		//   Tinem minte daca un nod este in stackul curent pentru a detecta un
		// ciclu
		bool inStack[MAX_NODES];
		memset(inStack, false, MAX_NODES);

		Node *current;
		stack<Node*> s;

		start->discovered = true;
		s.push(start);
		inStack[start->value - 'a'] = true;
		lastDiscoverTime++;

		while (! s.empty()) {
			//    Daca flag ramane false atunci nodul curent nu are vecini
			// nevizitati, deci il scoatem din stack si ii oferim discoverTime
			bool flag = false;

			current = s.top();
			for (auto node : current->neighbours) {
				if (node->discovered == true) {
					if (inStack[node->value - 'a'] == true) {
						return INT_MIN;  // Ne-a venit ciclul
					}
					continue;  // Sarim nodul deja vizitat
				}
				node->discovered = true;
				s.push(node);
				inStack[node->value - 'a'] = true;
				lastDiscoverTime++;
				flag = true;
				break;  // Am adaugat un nod in stack, iesim (DFS)
			}
			// Nu am gasit niciun vecin pentru nodul curent, deci il scoatem
			if (flag == false) {
				current->discoverTime = lastDiscoverTime;
				s.pop();
				inStack[current->value - 'a'] = false;
				lastDiscoverTime++;
			}
		}
		return lastDiscoverTime;
	}

	//   Vom aplica sortarea topologica asupra grafului curent stabilind
	// timpi de descoperire nodurilor
	// @return = false daca gaseste ciclu in graf, true pt success
	bool topologicalSort() {
		int lastDiscoverTime = 0;
		for (int i=0; i < MAX_NODES ; ++i) {
			if (nodes[i].discovered == false) {
				lastDiscoverTime = dfs(&nodes[i], lastDiscoverTime);
				if (lastDiscoverTime == INT_MIN) {
					return false;
				}
			}
		}
		//   Nu avem niciun ciclu deci putem sorta nodurile dupa timpii de
		// descoperire atribuiti
		sort(nodes, nodes + MAX_NODES, discoverTimeComp);
		return true;
	}

	//   Verifica daca litera 1 este mai mica sau egala decat litera 2 dupa
	// logica actuala din graf (folosita dupa ce a fost sortat cu noile reguli)
	bool compareValues(char value1, char value2) {
		for (int i = 0; i < MAX_NODES; ++i) {
			if (nodes[i].value == value1) {
				return true;
			}
			if (nodes[i].value == value2) {
				return false;
			}
		}
	}

	// Printeaza noul alfabet in fisierul de output primit ca parametru
	void printSortedNodes(ofstream &out) {
		for (int i = 0; i < MAX_NODES ; ++i) {
			out << nodes[i].value;
		}
	}
};

int main() {
/* 
	-=-=-=-=-=-=-=-=-= Declarari + citiri -=-=-=-=-=-=-=-=-=-=-=
*/
	ifstream in("permutari.in");
	ofstream out("permutari.out");

	int N;
	in >> N;

	list<string> words;
	string word;

	getline(in, word, '\n');  // Sarim primul rand la citire
	for (int i = 0; i < N ; ++i) {
		getline(in, word, '\n');
		words.push_back(word);
	}
	in.close();
/* 
	-=-=-=-=-=-=-=-=-= Formarea grafului -=-=-=-=-=-=-=-=-=-=-=
	  Stabilim noile constrangeri pe baza cuvintelor citite. In cazul in care
	avem 2 cuvinte "egale" iar primul este mai scurt atunci se incalca regula
	din cerinta, asa ca ne vom opri.
*/
	Graph graph;
	list<string>::iterator word1, word2;
	char c1, c2;

	word1 = words.begin();
	word2 = word1; ++word2;
	for ( ; word2 != words.end(); ++word1, ++word2) {
		for (int i = 0; ; ++i) {
			if (i < word1->size()) {
				if (i == word2->size()) {
					out << "Imposibil";
					out.close();
					return 0;
				}
			} else {
				break;
			}
			c1 = (*word1)[i];
			c2 = (*word2)[i];
			if (c1 != c2) {
				graph.addEdge(c1, c2);
				break;
			}
		}
	}
/* 
	-=-=-=-=-=-=-=-=-= Sortarea topologica -=-=-=-=-=-=-=-=-=-=-=
	  Incercam sa sortam topologic (primim false daca avem vreun ciclu). In caz
	de succes afisam noul aflabet
*/
	if (graph.topologicalSort() == true) {
		graph.printSortedNodes(out);
	} else {
		out << "Imposibil";
		out.close();
	}

	return 0;
}
